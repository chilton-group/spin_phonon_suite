Home
====

.. toctree::
   Installation <installation>
   Modules <modules/index>
   Guide <guide/index>
   Theory <theory/index>
   Contributors <contribs>
   :maxdepth: 3
   :caption: Contents:
   :hidden:

`spin_phonon_suite` is a python package devloped by the `Chilton Group <https://www.nfchilton.com>`_ for carrying out spin-phonon coupling calculations.