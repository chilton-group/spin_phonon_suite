Calculating vibrations/phonons
==============================

In spin-phonon coupling calculations the bath is decribed by a set of
independent (harmonic) oscillators.  ``spin_phonon_suite`` collects all
vibrational information in a HDF5-database, which is passed between the
different programmes via the ``-V`` flag. This database can be generated
from different quantum chemical input:

    * Gaussian ``*.log`` or ``*.fchk`` (enables the modification of atomic masses via ``--mass``)
    * Phonopy ``FORCE_SET`` file

.. code:: bash

    # Molecular vibrations from a gaussian calculation (pick one)
    spin_phonon_suite vib -V vibrations.hdf5 --gaussian_log gaussian.log
    spin_phonon_suite vib -V vibrations.hdf5 --gaussian_fchk gaussian.fchk

    # Solid-state phonons pre-processed by phonopy
    # Atomic displacement are generated for a cluster built from unit cells within a distance of 40Å of the central SMM
    spin_phonon_suite vib -V vibrations.hdf5 --poscar POSCAR --force_sets FORCE_SETS --q_mesh 5 5 5 --cluster_cutoff 40 --central_index 1

Note, ``--central_index`` is the ``POSCAR`` index of the central metal ion. The
cluster setup (last two arguments) need to match the arguments used in the run
of ``spin_phonon_suite cluster ...``.)

If you have calculated your phonons (forces) using a non-(1,1,1) supercell expansion then you will need to
specify this to ``vib`` using the ``--force_expansion`` optional argument.
