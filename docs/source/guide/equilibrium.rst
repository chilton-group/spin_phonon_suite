Equilibrium electronic structure
================================

The equilibirum electronic structure describes the system part of the
spin-phonon coupled system. It is most commonly computed at the CASSCF
level of theory.

Gas-phase calculations
----------------------

To generate an equilibrium input file for a gas-phase calculation,
i.e. without an environment, use `molcas_suite`, e.g.

.. code:: bash

   # Generate input
   molcas_suite generate_input opt.xyz Dy 9 7 10 +1 --output eq.input --high_S_only --skip_magneto --decomp RICD_acCD

   # Generate job submission file
   molcas_suite generate_job eq.input --scratch ~/scratch/_MY_SMM_ --submit_file eq.job


Including environment charges
-----------------------------

To generate an equilibrium input file which includes charges that represent the
environment, use `molcas_suite` with the ``--gateway_extra`` optional arguments
which specify the coordinates and definitions of the charges respectively from
the environment step, e.g.

.. code:: bash

   # Generate input 
   molcas_suite generate_input cluster.xyz Dy 9 7 10 0 --output eq.input --high_S_only --skip_magneto --gateway_extra 'basdir=$CurrDir' --decomp RICD_acCD --kirkwood 1e6 40 1

   # Generate job submission file
   molcas_suite generate_job eq.input --scratch ~/scratch/_MY_SMM_ --submit_file eq.job

Also, note the presence of the ``--kirkwood`` argument, which encapsulates the
sperical unit cell cluster generated in the previous step into a reaction field
cavity of commensurate radius. The dielectric constant is choosen to be
$\epsilon \rightarrow \infty$ (conductor), which in practise is an arbitrarily
large number. The reaction field is included to introduce counter charges which
depolarise the dipole moment (and higher multipole moments) of the cluster to
recover the Madelung potential in the cluster center. In the present example we
choose to only cancel the monopole (in case of a charged system) and dipole moment (l = 1). A suitable
cluster radius has to be determined by studing the convergence of the
electronic structure with growing cluster size, e.g. in steps of 10 Å.

Note:
    * Do not forget to revert subsitutions by diamagnetic analogues, e.g.
      Dy -> Y. This applies to all instances in both ENV and coordinate file.
      In solid state calculations, this should have ideally be already applied
      to the POSCAR in the previous step.
    * The index of the central metal centre (``1`` in the example) has to be
      determined from the ``cluster.xyz`` coordinate file and
      inserted into the ``molcas_suite generate_input`` command. Generally it
      is the only one without a predefined ``ENV.NNN`` basis label. 
    * Even though your SMM might carry a charge, condensed state systems should
      have a net charge of zero. Hence, this is the charge passed to
      ``molcas_suite generate_input``.
    * Due to the potentially high number of environment atoms, you might need
      to use a custom compiled version of ``OpenMolcas`` with increased
      ``mxAtom``, ``Mxdbsc`` and ``MxShll`` limits.
