FIRMS preparation for tau and firms_sim
=======================================

This secion of the documentation describes the methods for preparing the tau input files for FIRMS calcualtions.

Solid state
-----------

The Born-effective charge tensors (BEC tensors) are required along with the vibrational data obtained in phonopy. 
The BEC tensors are calculated by running DFPT in vasp on the optimised geometry POSCAR used in the phonon calculation.
To run this calculation run a SCF calcualtion using ``IBRION = -1`` and ``LEPSILON = .TRUE.`` in the INCAR file. This is 
implemented into ``vasp_suite`` by running the following command:

.. code:: bash

   # Calculate the BEC tensors and vibrational data
   vasp_suite phonon_calc --mesh <q point mesh> --supercell <force expansion> --bec


The ``BORN`` file can now be generated using phonopy by running the following command:

.. code:: bash

   # Generate the BORN file
   phonopy-vasp-born OUTCAR POSCAR --dim="a b c" --outcar


The dipole derivatives can then be passed onto to ``tau`` and ``firms_sim`` using the following command:

.. code:: bash

   # Generate the dipole derivatives for tau and firms_sim
   spin_phonon_suite prep firms --vasp_born <path to BORN file> -V <vibrations.hdf5> ...

   # Direct method
   spin_phonon_suite prep firms_direct --vasp_born <path to BORN file> -V <vibrations.hdf5> ...
   


Gas phase
---------
