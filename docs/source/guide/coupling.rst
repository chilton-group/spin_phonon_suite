Calculating Coupling Parameters
===============================

If spin-phonon couplings are evaluated via the linear vibronic coupling (LVC)
method, molecular gradients and nonadiabatic couplings (NACs) have to be
calculated with Molcas. Note, this calculation uses files located in the scratch
directory of the equilibrium calculation which is specified by the ``--old_path``
argument. To save computational, time the so-called "two-step procedure" is employed.
Gradient and NAC calculations are based on a single intial MCLR run within each
spin multiplicity which must be run in the same scratch directory as the
equilibrium calculation. As the intermediate MCLR files can be become quite large
(hundreds of GB), the gradient calculations can be alternatively carried out employing
the conventional single invocation of MCLR. Simply supply the flag ``--no-two_step`` to
turn off the default behaviour.

.. code:: bash

   # Generate lvc<S>_root<M>-<N>.input and lvc<S>_mclr.input Molcas inputs
   # S: index of the jobiph index of the respective multiplicity,
   # M/N: root indices
   spin_phonon_suite generate_input eq --ion Dy --old_path ~/scratch/_MY_SMM_

   # Alternatively, turn off two-step procedure
   spin_phonon_suite generate_input eq --ion Dy --old_path ~/scratch/_MY_SMM_ --no-two_step

   # Generate job script for "first step" of the MCLR run (not needed if two-step is turned off)
   molcas_suite generate_job lvc1_mclr.input --scratch ~/scratch/_MY_SMM_ --job_name mclr --submit_file lvc1_mclr.job

   # Generate job array with local scratch
   molcas_suite generate_job '${STEM}.input' --scratch '${TMPDIR}/_MY_SMM_/${STEM}' \
    --memory 16000 --disk 100000 --omp 4 --hpc_extra l='tmpdir_free=100G' \
    --array $(ls lvc*_root*.input | cut -d'.' -f1) \
    --exit_code 'rm -r ${WorkDir}' --job_name lvc --submit_file lvc.job

Notes:
    * You might want to adjust computational resources for memory and disk.
    * In case of multiple spin multiplicities the initial MCLR calculations may be submitted as a job array.
    * ``#$-l tmpdir_free=100G`` in the SGE header of ensures sufficient local scratch disk space.

Next, two HDF5-databases are generated:
    * The LVC parametrisation (``-L``)

.. code:: bash

   # Generate LVC model
   spin_phonon_suite lvc -L lvc.hdf5 --rassi eq.rassi.h5 --grad lvc*_root*.out

Finally, input files for ``tau`` are prepared:

.. code:: bash

   # Computation of spin-phonon coupling parameters (CFP derivatives)
   spin_phonon_suite prep tau -L lvc.hdf5 -V vibrations.hdf5 --ion Dy3+ --ground_level 6H15/2 --quax quax.hdf5

   # The QUAX argument is optional. If used, quax have to be extracted from eq.out or provided otherwise
   molcas_suite extractor -i eq.out -o quax.hdf5 --quax j

   # Computation of spin-phonon coupling parameters (direct method truncated to the lowest 16 roots)
   spin_phonon_suite prep tau_direct -L lvc.hdf5 -V vibrations.hdf5 --truncate 16

This prepares:
    * ``mode_energies.dat``, ``EQ_CFPs.dat`` and ``CFP_polynomials.dat`` (CFP derivatives)
    * ``mode_energies.dat`` and ``tau.hdf5`` (direct method)

