Environment
===========

In spin_phonon_suite, the environment around the central SMM (cations, anions, solvent) is represented
by point charges as opposed to molecular orbitals (MO). This is done to facilitate
faster and cheaper calculation of the spin-phonon coupling, and has been shown
to only result in a minor accuracy penalty over expensive full MO calculations. :sup:`[1]`

Two types of environments can be studied in spin_phonon_suite:
    1. Periodic environments which are highly ordered and regular, e.g. crystalline lattices and surfaces
    2. Amorphous environments which are essentially random and irregular e.g. frozen solutions or powders

Periodic (ordered) environments
-------------------------------

Here, optimisation and frequency calculations are carried out on a unit cell
using periodic density functional theory (DFT). This unit cell is then translated
multiple times and used to build a cluster in which the spin-phonon coupling is
studied.

Owing to its highly ordered nature, this cluster will contain many copies of
the same molecules/ions, and it is most sensible to calculate charges for
the unique entities and then map these onto every occurrence of a given entity.

Through ``env_suite`` we use the CHarges from ELectrostatic Potentials using
a Grid-based method (CHELPG) process to determine charges that represent the
DFT molecular electrostatic potential of a given molecule in the cluster.
The interaction between entities is neglected in our approach,
and so the CHELPG charges are determined for isolated entities.

Step 1: Calculating CHELPG charges
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``charges`` subprogram available in the ``env_suite`` command line
interface takes an optimised primitive unit cell (in the form of a ``POSCAR``
file), generates gaussian inputs for each unique entity, parses the
corresponding outputs and maps the charges back onto the atoms of the primitive
unit cell.

e.g. a Dysprosocenium system with no solvent

.. code:: text

    env_suite charges --gen POSCAR

results in the following terminal output

.. code:: text

    ******************************
    The unique entities are
    C34H58Y
    BC24F20
    ******************************

    Input file written to gaussian/C34H58Y_1.com
    Generate submission script with gaussian_suite gen_job gaussian/C34H58Y_1.com NCORES 

    Input file written to gaussian/BC24F20_1.com
    Generate submission script with gaussian_suite gen_job gaussian/BC24F20_1.com NCORES 

    Charge and multiplicity set to 99 in all .com files
    Please replace with actual values before submitting  

For each unique entity a Gaussian input (``.com``) file has been produced
specifying a single CHELPG calculation. Users MUST edit each ``.com`` file
to specify the charge and spin multiplicity of each entity.

By default the CHELPG calculations use PBE+D3 with Stuttgart 1997 ECPs for
metal ions, so some user configuration may be needed if this default is not desired.

Additionally, if closed shell analogues are to be used, e.g. Dy --> Y, then this swap
can either be carried out by editing the ``POSCAR``, or by editing the ``.com`` file.

When studying high symmetry systems, it is likely that the symmetrically independent
atoms will not constitute a molecule on their own. Consider a perfectly tetrahedral
coordination complex, only a single ligand and metal ion is needed to reproduce the
entire structure. This symmetry *should* be reflected by the CHELPG charges which
are carried out on the entire molecule, provided Gaussian employs the relevant point
group in the calculation. However, this is not guaranteed, and so ``charges --parse``
symmetrises (and neutralises) the charges in the unit cell.

Step 2: Mapping CHELPG charges onto the unit cell
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Following the successful calculation of the CHELPG charges, the ``charges --parse``
subprogram can be used to map the unqiue entities back onto those of the unit cell.
This step must be run in the same directory as ``charges --gen``.

e.g. following on from the above example

.. code:: text

    env_suite charges --parse POSCAR

Step 3: Generating the point charge cluster embedding of the central SMM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Finally, the point charge cluster with the SMM embedded in the centre is built.
The ``--cluster_cutoff`` provides the cutoff radius for spherical clusters
made from unit cells which lay at most one cutoff radius away.
The atomic index of the metal ion is provided from which the QM-region, the SMM
under investigation, is built and embedded in the centre of the cluster.

.. code:: text

    env_suite cluster --cluster_cutoff 40 --poscar POSCAR --central_index 1 --from_central

The most important files created in these steps are:
    1. ``charges.dat`` - Environment charges in the same order as the POSCAR atoms
    2. ``ENV`` - Custom molcas basis set specification for environment charges
    3. ``cluster.xyz`` - Molcas XYZ file with predefined environment basis set labels

Of these, 2 and 3 are used to specify the system coordinates and environment charges in the Equilibrium and Coupling steps.

Amorphous Environments
----------------------
As an example the modelling of dysproscenium in an amorphous DCM solution environment using QM/MM is demonstrated.[2]

    1. Assign suitable atom types.
    2. Create solvent box.
    3. Place SMM into the solvent box.
    4. NPT solvent+cell equilibration at finite temperature. (SMM coordinates are kept frozen)
    5. NPT solvent+cell equilibration at absolute zero. (SMM coordinates are kept frozen)
    6. Joined environment+SMM optimisation.

All SMM and environment atoms need to be assigned an appropriate atom type found in the ``*.prm`` force field databases supplied by Tinker. Using a generic "all-atoms" force field like OPLS-AA, most common atoms types should be available. While the MM potential of the environment (i.e. the solvent) needs to be fully parametrised, only non-bonded interactions are required for the system (i.e. the SMM) to describe the environment-system interactions assuming no bonds between the two subsystems.

Example ``dcm.xyz`` file of a single DCM molecule assigned with OPLS-AA atom types:

.. code:: text
    
     5  DCM
     1  CT     2.290   4.080   2.260   801     2     3     4     5
     2  HC     2.930   3.280   1.880   802     1
     3  HC     1.260   3.740   2.170   802     1
     4  Cl     2.610   4.140   4.010   800     1
     5  Cl     2.650   5.680   1.580   800     1

A key file is supplied to specify additional simulation parameters. In case of the DCM molecule, refined force field parameters have been derived which may be specified in the ``dcm.key`` file.

.. code:: text

    OPENMP-THREADS 1
    parameters oplsaa.prm

    # overwrite DCM parameters
    charge 800 -0.1
    vdw 21 3.4000 0.3000

Next, a periodic box is filled with solvent molecules at the appropriate density using ``xyzedit``. It is advisable to avoid excessive overcrowding or vacuum bubbles in the solvent box to obtain a good initial conditions for the subsequent equilibration. Here 100 DCM molecules are filled into a cubic box of length 20Å.

.. code:: text

   xyzedit dcm.xyz -k dcm.key  # option 22
   mv dcm.xyz_2 solvent.xyz

The SMM is placed into the centre of the solvent box by first translating the centre of mass (or optionally the central ion) to the origin and then "soaking" the SMM in the box of solvent using ``xyzedit``. IMPORTANT: The resultant xyz file may not include the box dimensions between the number of atoms and the cartesian coordinates and you may need to add this manually. 

.. code:: text

   xyzedit smm.xyz -k box.key  # options 13/14 followed by 23
   mv smm.xyz_2 box.xyz

``box.key`` must now also contain the definition of the atomic SMM charges derived earlier. Additionally intermolecular SMM interactions are turned off and the SMM atoms (1-93) are frozen.

.. code:: text

    ...
    # define SMM charges
    charge -1 0.999025
    charge -2 0.628114
    ...
    # turn off inter-system interactions
    group 1 -1 93
    group-select 1 1 0.0

    # fix system coordinates
    inactive -1 93

Subsequently, the solvent coordinates and cell parameters are equilibrated in two steps by running NPT dynamics simulations under ambient pressure.

.. code:: text

    #                          steps     dt   NPT  temperature
    dynamic box.xyz -k box.key 50000 1.0 0.05 4    5.0 1.0
    #                                dt between saves  pressure

First, the temperature is helt at a finite value (e.g. 5K) to allow thermal motion of the solvent molecules during the initial phase of the equilibration. Then, the target temperature is set to absolute zero (in practise to something arbitrarily small such as 0.001K). The key file is expanded by the MD integrator as well as the barostat and thermostat settings. The time constant of the thermostat is chosen to be 0.1ps for the initial equilibriation and 2.0ps to allow for a slow approach to absolute zero, and hence the final solvent coordinates and box dimensions of the frozen system.

.. code:: text

    ...
    # MD integrator
    integrator verlet
    # Baro-/thermostat settings
    barostat berendsen
    tau-pressure 1.0
    thermostat berendsen
    tau-temperature 0.1

Finally, the energy is minimised in respect to all atoms in the box. To speed up convergence and enhance computational efficiency, the SMM and solvent subsystems are relaxed iteratively in an alternating fashion. Hence, only the SMM relaxation involves expensive QM/MM evaluations while the solvent relaxation only requires the cheap evaluation of the MM potential. Here, we choose 10 steps of SMM relaxation followed by a complete relaxation of the solvent. The gaussian keyword line takes the following form:

.. code:: text

    #p ONIOM(External="script/mygau o g09 pbepbe/GenECP basis_file"/GenECP:
             External="garleek-backend --qm gaussian_09d --mm tinker --ff system.key --box 20.0 20.0 20.0"/GenECP)
       NoSymm opt=(nomicro,ReadOptimize,MaxCycles=10) geom=connectivity

Here, the first external call runs a regular Gaussian DFT calculation enabling the usage of empirical dispersion which is not available for an internally run DFT ONIOM layer. The second external call evaluates the MM potential  via Tinker by invoking the Garleek interface. In order to enable periodic boundaries the current box dimensions (change me) are specified. Note, this feature is only available via the modified Garleek fork available `here <https://gitlab.com/TheTschakk/garleek>`_. Further, the above command requires the presence of various file in the current directory. The ``basis_file`` contains the basis set definitions of the SMM atoms to be read by GenECP. The ``system.key`` file specifies additional options for the evaluation of the MM potential as defined above.

.. code:: text

    #p ONIOM(External="script/mygau o g09 pbepbe/GenECP basis_file"/GenECP:
             External="garleek-backend --qm gaussian_09d --mm tinker --ff system.key --box 20.0 20.0 20.0"/GenECP)
    NoSymm opt=(nomicro,ReadOptimize,MaxCycles=10)
    geom=connectivity

In order to control the iterative procedure, scripts and input templates are available in the group.
Subsequently, a harmonic frequency analysis is carried out by replacing the ``opt`` keyword by the ``freq`` keyword.
Note that in an ONIOM calculation basis set definitions need occur three times at the bottom of the input file.
Further, MM atomtypes need to be stated in the element column of the coordinates like ``Dy-647``.


Note:
    * Do not change the ordering of atoms in any of the subsequent steps!

References
----------

[1] J. Staab and N. Chilton, Analytic Linear Vibronic Coupling Method for First-Principles Spin-Dynamics Calculations in Single-Molecule Magnets. J. Chem. Theory Comput. 2022, 18, 6588–6599.
[2] J. Staab, Md. K. Rahman, N. Chilton, Intramolecular bridging strategies to suppress two-phonon Raman spin relaxation in dysprosocenium single-molecule magnets. Phys. Chem. Chem. Phys. 2024, 26, 17539.
