.. _guide:

Guide
=====


.. toctree::
   :maxdepth: 1
   :hidden:

    Vibrations/Phonons <frequencies>
    Environment <environment>
    Equilibrium <equilibrium>
    Couplings <coupling>
    FIRMS <firms>

Using `spin_phonon_suite`, spin-phonon coupling calculations can be carried out in three or four steps

1. Optimisation of system and calculation of vibrational/phonon modules
2. Calculation of environment representation (optional)
3. Calculation of equilibrium electronic structure
4. Calculation of spin-phonon coupling parameters
