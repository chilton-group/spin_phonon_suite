Installation
============

Install `spin_phonon_suite` using `pip`::

    pip install spin_phonon_suite


Molcas Compilation
==================

By default, Molcas supports up to 5000 atoms. However, including environment charges in a spin-phonon coupling calculation means that the number of atoms is often much larger than this limit.

To increase the maximum number of atoms, some small changes need to be made to the Molcas source code before compilation.

In `src/Include/Molcas.fh`, `mxAtom` and `Mxdbsc` need to be increased to **99999**:

.. code:: fortran

    #ifndef _DEMO_
          Integer, Parameter :: mxAtom = 99999
          Integer, Parameter :: mxroot = 600
          Integer, Parameter :: mxNemoAtom = 200
          Integer, Parameter :: Mxdbsc= 99999
    #else

Additionally in `src/gateway_util/soctl_seward.F90`, two format specifiers need changing to support 5 digit numbers, changing I4 to **I5**:

.. code:: fortran

                  if (.not. Primitive_Pass) then
                    write(isymunit,'(13(I5,4X))') iSO_,mdc,LVAL(lculf),MVAL(lculf),nIrrep/dc(mdc)%nStab, &
    ...
    ...
                  if (Shells(iSh)%Aux .or. Shells(iSh)%Frag) cycle
                  write(isymunit,'(13(I5,4X))') iSO,mdc,LVAL(lculf),MVAL(lculf),nIrrep/dc(mdc)%nStab, &





