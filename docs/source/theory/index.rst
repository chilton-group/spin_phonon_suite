Theory
======

.. toctree::
   :maxdepth: 2
   :hidden:

    Vibrational Modes and Phonons <vibrations>
    LVC <lvc>
    Coupling Strength <strength>
    Symmetries <symmetries>
