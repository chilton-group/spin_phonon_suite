Symmetries
==========

Due to the numerical nature of ane electronic structure calculation, certain symmetries might not be exactly reflected in the observables, e.g. degeneracies in the Hamiltonian due to point-group symmetry, time-reversal symmetry, etc. As a consequence, enforcing these symmetries might be required to revover the correct physics of a given system.

Time-reversal symmetry
----------------------

Especially in physical systems with spin, time-reversal symmetry manifests itself in various ways. :sup:`[1]` Most famously, under a time-reversal even Hamiltonian (true for all molecular Hamiltonian terms inlcuding Spin-orbit coupling) and half-integer spin, electronic states occur as Kramers time-reversal conjugate pairs of degenerate energy eigenvalue.

Generally, matrix elements of time-reversal even (odd) Hermitian operators show the distinct upper (lower) sign phase relation:

.. math ::
   \langle \beta | A | \alpha \rangle = \pm \langle \tilde{\beta} | A | \tilde{\alpha} \rangle^* = \pm \langle \tilde{\alpha} | A | \tilde{\beta} \rangle

where the tilde denotes time-reversed states via action with the time-reversal conjugation operator: :math:`\Theta | \alpha \rangle = | \tilde{\alpha} \rangle`, and Hermicity is used in the last equality. Generally, a time-reversed states does not strictly equal its time-reversal conjugate, but instead proportionality holds introducing a phase factor. In case of the components of a Kramers conjugate pair enumerated by :math:`n`: :math:`\Theta | n \rangle = \exp(i \phi_n) | \bar{n} \rangle`, and :math:`\Theta | \bar{n} \rangle = \exp(i (\phi_n + \pi)) | n \rangle`, where the bar denotes the time-reversal conjugate state. This follows from the well-known result for half-integer spin systems, that time-reversing a state :math:`| n \rangle` twice yields :math:`-| n \rangle`.

.. math ::
   \Theta (\Theta | n \rangle) = \Theta (e^{i \phi_n} | \bar{n} \rangle) = e^{-i \phi_n} (\Theta | \bar{n} \rangle) = e^{-i \phi_n} (e^{i (\phi_n + \pi)} | n \rangle) = - | n \rangle

Note, that usually a simple phase convention is chosen such that, e.g. :math:`\Theta | n \rangle = | \bar{n} \rangle` and :math:`\Theta | \bar{n} \rangle = -| n \rangle`. However, the components of each Kramers doublet come from the ab initio calculation in an arbitrary linear combination. Here, we choose to only correct the spurious time-reversal asymmetry while preserving the linear combination, and hence stick to the more general phase convention.

Plugging the definitions of the time-reversed states in terms of their time-reversal conjugates into the general phase relation, it follows for the time-conjugate components of Kramers doublets:

.. math ::
   \mathrm{I}: \langle m | A | n \rangle = \pm e^{i \Phi_{mn}} \langle \bar{n} | A | \bar{m} \rangle,

and

.. math ::
   \mathrm{II}: \langle m | A | \bar{n} \rangle = \pm e^{i (\Phi_{mn} + \pi)} \langle n | A | \bar{m} \rangle,

where :math:`\Phi_{mn} = \phi_m - \phi_n`. In case of :math:`n = m`, the usual relations between matrix elements in the subspace of a time-reversal conjugate pair are recovered, i.e. for time-reversal even (odd) observables, expectation values are equal in magnitude and sign (equal in magnitude and opposite in sign) as well as off-diagonal elements are exactly zero in case of time even operators.

.. math ::
   \langle n | A | n \rangle = \pm \langle \bar{n} | A | \bar{n} \rangle,

and

.. math ::
   \langle n | A | \bar{n} \rangle = \mp \langle n | A | \bar{n} \rangle = \begin{cases} 0 & \text{even} \\ \langle n | A | \bar{n} \rangle & \text{odd} \end{cases}

The following algorithm can be employed to enforce time-reversal symmetry in finite precision numerical calculations. Iterating over the pair of indices :math:`m` and :math:`n`, enumerating Kramers doublets, the four states :math:`\{|m\rangle, |\bar{m}\rangle, |n\rangle, |\bar{n}\rangle\}` need to fulfill the relations I and II, which determines both magnitude (equal within each relation) and phase (:math:`\Phi^\mathrm{I}_{mn} = \Phi^\mathrm{II}_{mn}` among the equations).

Hence, considering the polar representation of theses matrix elements :math:`\langle m | A | n \rangle = |A_{mn}| \cdot \exp(i \theta_{mn})`, purely even (Hamiltonian, spin-phonon coupling) or odd (spin and orbital angular momentum) operators may be "time-reversal symmetrised" by firstly averaging the magnitudes :math:`|A_{mn}|` and :math:`|A_{\bar{n}\bar{m}}|` as well as :math:`|A_{m\bar{n}}|` and :math:`|A_{n\bar{m}}|`.
Secondly, the spurious difference :math:`\delta = \Phi^\mathrm{I}_{mn} - \Phi^\mathrm{II}_{mn} = \theta_{mn} - \theta_{\bar{n}\bar{m}} - (\theta_{m\bar{n}} - \theta_{n\bar{m}} - \pi)` between two equations is removed by evenly adjusting the phase of the matrix elements according to their sign in the definition of :math:`\delta`: :math:`\theta_{mn} \rightarrow \theta_{mn} - \delta/4`, :math:`\theta_{\bar{n}\bar{m}} \rightarrow \theta_{\bar{n}\bar{m}} + \delta/4`, :math:`\theta_{m\bar{n}} \rightarrow \theta_{m\bar{n}} + \delta/4` and :math:`\theta_{n\bar{m}} \rightarrow \theta_{n\bar{m}} - \delta/4`

EXPERIMENTAL SECTION
--------------------

In case of integer spin systems, this procedure is not applicable as the energy eigenstates do not occur in time-reversal conjugate pairs. A general algorithm involves the averaging of the matrix elements of the full Hermitian operator with its "phase-adapted" explicitely evaluated time-reversed counterpart:

.. math ::
   A \rightarrow \frac{1}{2} ( A \pm \Theta A \Theta^{-1} ) = \frac{1}{2} ( A \pm e^{-i \pi J_y} A^* e^{i \pi J_y}  )

where :math:`\Theta = U K` is the antiunitary time-reversal operator involving a unitary component :math:`U = exp(-i \pi J_y)` and the antilinear complex conjugation operator :math:`K` giving rise to :math:`A^*`. For instance consider the action of :math:`\Theta` on the matrix elements of the total angular momentum in the canonical basis (enforcing phase conventions that leave :math:`J_x` and :math:`J_z` purely real and :math:`J_y` purely imaginary):

.. math ::
    U K \begin{bmatrix} J_x \\ J_y \\ J_z \end{bmatrix} K^{-1} U^\dagger =
    U \begin{bmatrix} J_x \\ -J_y \\ J_z \end{bmatrix} U^\dagger =
    \begin{bmatrix} -J_x \\ -J_y \\ -J_z \end{bmatrix}

thus proving the time-reversal odd nature of angular momentum. Here, the unitary operator introduces a half rotation around the y-axis, resulting in a total action of the time-reversal operator of permuting states of opposite :math:`m_j` and introducing a complex phase :math:`\Theta |j,m_j\rangle = i^{2m_j} |j,-m_j\rangle`.

Note, that the latter algorithm might be less rubust as it is subject to errors stemming from the incompleteness of the time-reversal conjugate subspace bases in the truncated ab initio states. In case of Kramers systems, this is obviously avoidable by including states in Kramers-conjugate pairs, but the situation might be less straight-forward in non-Kramers systems where time-reversal conjugate pairs of states might be mixed thoughout the energy eigenbasis and subsequently arbitrally truncated upon choice of basis size.

[1] J. J. Sakurai, S. F. Tuan, Modern Quantum Mechanics (Addison-Wesley Pub. Co, rev. ed., 1994).
