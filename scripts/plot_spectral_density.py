# plots the spectral density from a spin_phonon_suite prep tau_direct calculation
# USAGE: python3 plot_spectral_density.py in folder containing
# mode_energies.dat and coupling_strength.dat

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

modeEnergies = np.loadtxt('mode_energies.dat', skiprows=1)
couplingStrengths = np.loadtxt('coupling_strength.dat')
fwhm = 10  # fwhm, in cm-1
xmax = 300


def spectrum(E, cStr, sigma, x):
    gE = []
    for Ei in x:
        tot = 0
        for Ej, os in zip(E, cStr):
            tot += os * np.exp(-(Ej - Ei) ** 2 / (2 * sigma**2)) \
                / ((2 * np.pi)**0.5 * sigma)
        gE.append(tot)
    return gE


fig, ax = plt.subplots(figsize=(6, 4))
ax.set_xlabel("Wavenumber ($\\mathrm{cm}^{-1}$)", fontsize=14)

ax.set_xlim(0, xmax)
ax.set_ylabel("Spectral density ($\\mathrm{cm}^{-2}$)", fontsize=14)

x = np.linspace(0, xmax, num=1000, endpoint=True)

# sigma = fwhm / (8 ln(2))**0.5
sigma = fwhm / 2.35482004503

gE = spectrum(modeEnergies, couplingStrengths, sigma, x)
ax.plot(x, gE)

plt.tight_layout()

plt.savefig('coupling_strength.pdf')
