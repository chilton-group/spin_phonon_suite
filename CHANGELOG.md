# CHANGELOG


## v1.8.2 (2025-03-12)

### Build System

- Update dependencies
  ([`e590e76`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e590e76c1e897ce5a05b3e588254a0e625a53ce2))

- Update numpy version dependency
  ([`51c4843`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/51c4843096b801d4425a91a50767d99024bf7664))

### Continuous Integration

- Update build variables
  ([`47b4606`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/47b4606143df561227be1363170d0951f26d97de))


## v1.8.1 (2024-10-25)

### Bug Fixes

- Correct typo in LVCMagneticSusceptibility
  ([`9f8dd4c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9f8dd4c5e8e8c27c36b9318472fbe4fc0e7898ba))

"spin_phonon_suite eval --sus" did not function due to a reference to spin and orbital angular
  momentum operators in the spin-orbit coupled basis as "soc_spin" and "soc_angm", where these are
  referred to as "sos_spin" and "sos_angm" when defined. This is corrected. Magnetic susceptibility
  tensors calculated by spin_phonon_suite at a given equilibrium geometry and temperature now agree
  with those from OpenMolcas' SingleAniso.


## v1.8.0 (2024-10-03)


## v1.7.5 (2024-08-15)

### Bug Fixes

- Strength --help message
  ([`b8c44bf`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b8c44bf3cbd934734b53bce23c1a894224cd1958))


## v1.7.4 (2024-08-13)

### Bug Fixes

- Update plot_spectral_density.py
  ([`d6b42cd`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d6b42cdcb6d1dc5dbc30aabb835f28f5b88e6fdc))


## v1.7.3 (2024-07-29)

### Bug Fixes

- Add script to plot spectral density
  ([`78d9df0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/78d9df0875ba6051ee941ba51bb388a19f639150))


## v1.7.2 (2024-07-25)

### Bug Fixes

- Removed import of function that does not exist
  ([`98e16e5`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/98e16e5788070952cc2dc1809d38d115f636d3e4))

Removed the import of an old function in cli.py that no longer exists and cannot be imported

### Documentation

- Add amorphous envs
  ([`96abcfa`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/96abcfa2f7010d063a45445939b61593706da9e9))

Tutorial for amorphous environments.

- Remove CHOF option
  ([`43dedbb`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/43dedbbfb121dc3c7428562aeff89ffd2eb24afd))

CHOF option likely leads to incorrect NACs. Removing it from the docs.


## v1.7.1 (2024-06-17)

### Bug Fixes

- Remove deleted function import
  ([`2512d74`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2512d744a183ab1c23b51993c4a25051dc9ad3b5))


## v1.7.0 (2024-06-17)

### Bug Fixes

- Bug fixes for implementation
  ([`f251ce6`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f251ce65016bee49329253a6c6f2581bd24c78c1))

fixing bugs whilst testing the code, Normalise the derivatives per unit cell. enforce the
  derivatives match up with the shape of the vectors.

- Calc with q-mesh
  ([`31cd4c1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/31cd4c18b803bc19184fad02bb89fe4eb281c2c3))

### Features

- Electric field model
  ([`8e22804`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/8e228047cba925e93e90aabfc3945e80b08505d2))

Implementation of the electric field model

- Electric field model implementation
  ([`25ce741`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/25ce74118494d3274d259ee85e7fdf3d1825ccf8))

- Neutron weights
  ([`d361cb8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d361cb82c2838514af1138626dbb040bac1b40a3))

Computes per-mode neutron weighting factors for construction of neutron weighted DOS.

- Phonopy derivatives (WIP)
  ([`9f61c93`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9f61c93f082b8fba41995b2832e04907a6636bcc))

Implemented the calculation of dmu/dQ for testing.

- Store atomic numbers
  ([`d540e05`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d540e05dcb2b94410af2aac2df47fbb8569faeb2))

Store atomic numbers when possible for nwDOS calcs.

### Refactoring

- Clean up
  ([`961233e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/961233e5a190145c77197a7a4b99546bb975cf68))

Removed redundant code, renaming of variables


## v1.6.2 (2024-04-17)

### Continuous Integration

- Fix pyproject.toml [skip ci]
  ([`3a4bd20`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3a4bd20f4107bebf492cf0e43a2810b79af4c58c))


## v1.6.1 (2024-04-17)

### Continuous Integration

- Fix .toml syntax for version paths [skip ci]
  ([`df696d2`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/df696d2abf44c4cdde86d4c777824e9dd526db23))


## v1.6.0 (2024-04-17)

### Documentation

- Updated docs
  ([`09db1f5`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/09db1f5c6f8a00855e4252161ee31d91b227a3e1))

updated solid state workflow to used l=1 in reaction field calculations in line with molcas giving
  divergences in the NACs when using higher l.

### Features

- Absolute value of imaginary frequencies
  ([`43997fc`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/43997fc6883421aab91833f825bcf2b574761c4e))

- Store absolute value of imaginary frequencies
  ([`864bda4`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/864bda44e7648e74fa26a26e5db0a0eba648687e))


## v1.5.0 (2023-12-19)

### Documentation

- Add theory about symmetry
  ([`322ee65`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/322ee6541fb2f5516710f09c5047926df0c578d1))

TR symmetrisation theory corresponding to issue 44 of the tau repo.

- Add theory about symmetry, with edits
  ([`897643f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/897643fff007b97c1bfdc69d6ff97314bf1cc7d1))

TR symmetrisation theory corresponding to issue 44 of the tau repo.

- Remove custom molcas version in suggested molcas_suite commands
  ([`6c98c04`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6c98c04a856265061b68377014d02db898c0de09))

- Update install docs
  ([`ee71a66`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ee71a66c8ff8238b9cedc1c36a60c311545eedb2))

update install docs to include documetation on modifying molcas to support more than 500 atoms

### Refactoring

- Eval cli interface
  ([`eb7fef4`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/eb7fef44d420a16ed5ec3eab09e831ebd18f1e05))

Restore -L arg for lvc data and --geom for iteration.

- Restore correct behaviour for empty --geom arg
  ([`a22b464`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a22b46405f81249de8b331566b977e3c1c5d6b6d))

Empty --geom arg triggers evaluation at eq geom.


## v1.4.1 (2023-09-29)

### Bug Fixes

- Numerical precision
  ([`358008a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/358008a0a04b21b4f5e8d22318921a39e47728f2))

precondition spin-free energies before SOC


## v1.4.0 (2023-09-18)

### Bug Fixes

- Minor fixes
  ([`64e5d7d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/64e5d7d0b15cd4f1e2fd59c65163d082ba15c621))

Minor post-test bug fixes.

### Features

- Default removal of imaginary modes
  ([`5b7c35b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/5b7c35b33a85e763b88635f8d0ae9aca054bedcd))

Imaginary modes are discarded by default (can be overwritten).


## v1.3.3 (2023-09-18)

### Bug Fixes

- Labeling of sf Hamiltonian
  ([`d7200bb`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d7200bba1901b9090b0c634882f4047b5050e29f))

When requesting the Hamiltonian in the SF basis. The spin multiplicity must be used as a label.


## v1.3.2 (2023-08-28)

### Bug Fixes

- Import statements
  ([`b22ad7b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b22ad7b885f6c8a3204df173f2d92f0df7021d49))

Fix import statements + docs

### Build System

- Include env_suite dependency
  ([`b869418`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b869418d650e191bf3ff3ac7f54d27eb10bd81f7))

Now depends on env_suite.

### Refactoring

- Delete cells code
  ([`464d00b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/464d00b5c63a138fff7b48d94ab2af5ddddf9f69))

clean up code base

- Migrate env code
  ([`729e8ee`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/729e8eeda88f656042dee2de22e5d0a60a88cca6))

migrate cluster and charge programs and adapt docs.


## v1.3.1 (2023-08-03)

### Bug Fixes

- Minor bug in vibration program
  ([`c330574`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c3305746d68baace2b2bcffffb4e787eda8472f8))

Name of central index variable had not been changed appropriately. Raises loud error.


## v1.3.0 (2023-08-01)

### Features

- Muliple index specification of the qm-region
  ([`ee171d5`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ee171d55f223236562a5f0cbd96d6a31772ec8d6))

Expand qm region selection to multiple molecules.


## v1.2.0 (2023-06-09)

### Bug Fixes

- Graph generation
  ([`6972654`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/69726540fd67fe0633f8e778bd46d4a1acfed2f0))

consistent interface

### Refactoring

- Remove coordinates.py
  ([`b6b7895`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b6b7895c2f31ee585ae4235a560bbb200dc5f8ee))

old legacy code, bye bye


## v1.1.0 (2023-06-05)

### Bug Fixes

- Fomula format
  ([`f629932`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f62993246fc7f947d22a6a4dcb952bae0b790136))

Correct ordering + remove spurious ones.

- Qm_region keyword
  ([`67fd518`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/67fd518102199406bfa98ad2db88a8889cfb26dc))

Broken backend.

- Use symmetry equilvalence index as identifier
  ([`37d2f19`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/37d2f19e3c100fe43ba78264d67a1182d39521e6))

... when determining graph isomorphisms

### Build System

- Add pymatgen to dependencies
  ([`7b1da4b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7b1da4bdbc0f598fa8865afcc5d7b4b425292bd9))

### Features

- Print error when charges are not present
  ([`8723708`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/87237087e86d3d13208895c122dbbf52c3deaba1))


## v1.0.0 (2023-06-01)

### Bug Fixes

- Q-point filtering
  ([`2887e66`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2887e663c9554e9acf7dfac75b8fbef32b79c809))

Correct hasing is needed for "in" operator for sets. Resorting to list. wip

### Documentation

- Expand on reaction field section
  ([`9fb6023`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9fb60231211f6043bd6517299605c56cbaf0c8a9))

Expand documentation with reaction field instructions.

### Features

- Add q-point class
  ([`0a2b39f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0a2b39fd7738260707c2d9a12ea94586c08034f0))

q-point class to handle filtering and unique-ness of q-points.

- Old supercell morphology for backwards compatibility
  ([`f01ef58`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f01ef5860c2a4e8b1f130f7e9b79c8c77093f1de))

Brings back old supercell morphology. To be removed in the future.

### Refactoring

- Clean up q-point sampling
  ([`ddb924f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ddb924fd8bc91744977914ebffc2532150235b32))

- Improved q-point filtering
  ([`0906e75`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0906e75afdc0fd2b5394cc893c97ee9d595cc26f))

Instead of preset collection of positive q-points, employ uniqueness filter.

- Inverse q-point label
  ([`dec24e0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/dec24e0e82e82beda0fd41ddd387c4c1eefcf2a8))

Label mode associated to imaginary displacement with negative q-point.


## v0.25.0 (2023-05-28)

### Bug Fixes

- Q-filter for boundary points
  ([`892aba8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/892aba884afcd1413580148f7fdfc425bd139553))

### Documentation

- Update documentation
  ([`74aac75`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/74aac75a692de53065500cd7b4ddc9efd12a89ac))

### Features

- Change normal mode convention
  ([`6417bda`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6417bda15869b276a3683f8836b6bbd44de91701))

The normal mode operators are written as b + b^t. All numeric constants are absorbed into the
  displacement amplitudes / couplings as descriped in the rsc review.

- Choice of gamma centering
  ([`9b05918`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9b059185aeb9a92321b49885030ec66f472daa99))

Also, correct treatment of boundary q-points

- Complete phono3py hdf5 interface
  ([`17d9b02`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/17d9b021e9fc8d19c01f300beccf59bd8b7b491d))

- Enable spherical rf method
  ([`37ffcd7`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/37ffcd713cae1f4a6dbd90a548b1f8f603c1c922))

Implement and clean up charge gen/parsing and implement rf method

- Fix prev commit
  ([`2d92610`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2d92610d9dc6c3ff1a8d6508e875a71e7783cd0d))

### Refactoring

- Avoid redundant info
  ([`e757d09`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e757d09030833ed5dadf63c4a59eb79fdb6e008f))

Each mode (real + imag part) is only included once which is compensated by a factor of sqrt(2).
  Reduces number of modes to N_qpoints x 3N.

- Clean up cluster code
  ([`81381cf`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/81381cf89175e061ecbbea69c632a140435c4854))

- Experimental refactor for complex displacements
  ([`ff3217d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ff3217d8e11e90e2a6bab84eb4beffd820ad7bfb))

Complex displacements can be used, but interface to tau woud need to take complex CFP derivatives.
  Simply taking the abs value of CFP derivatives does not seem right as sign info gets lost for
  building the electronic coupling elements. Would work with the direct coupling elements. Now
  reverting to passing real and imag part separately.

- Factor out qpoint mode generation
  ([`eaea24e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/eaea24ec896cb98103378f9b610de0cdf6ad50f5))

- Reduce number of q-points
  ([`513cafe`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/513cafe2bc61e224970384dce95c9250f732896b))

Previously, the number of q-points was doubled to treat real and imaginary component of coupling
  separately. Now this has been resolved by only integrating over "positive" q-points and providing
  the correctly normalised coupling of pairs of modes (+q/-q). This restricts q-point integration to
  "symmetric" meshes.

- Separately pass real and imag part at off-gamma points
  ([`0ea7488`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0ea748815b5130d8f64c79518d205fe964d7a7c8))

Off-gamma vibrational displacements are now generated separately for real and imaginary part. This
  saves head aches in the coupling calcs down the line. Leads to formally double the number of
  vibrations.


## v0.24.1 (2023-04-26)

### Bug Fixes

- Bug introduced in earlier fix
  ([`3d6fa45`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3d6fa453dd6c9cb88fdc10c6c6ca42480336735b))

Gaussian's normal modes need to be divided by the sqrt of the reduced mass to remove normalisation
  and not by the bare reduced mass. This mistake never made it into any of our calculations!

### Documentation

- Fix prep tau arguments
  ([`0ff836c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0ff836cf768aedfc0cdaae9443aed106b1b525c4))


## v0.24.0 (2023-04-15)

### Bug Fixes

- Imaginary and real displacements for solid state phonons
  ([`ce0229f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ce0229f5fa9d78d7a79b8fa812c585a6ffc0ac84))

Phase info and normal mode magnitude whwere discarded when taking the real part of the atomic
  displacements. To be clean up in a future merge request.

- Traceless argument
  ([`21a693a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/21a693a3856999d96cfdf3fbd10af7b182189ae3))

### Refactoring

- Remove reduced mass
  ([`76664a9`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/76664a9dd7fc4821124a51dbed10b096d68ddeb1))

Reduced masses confuse more than they help. Only legacy reasons due to Gaussian conventions. They
  are now factored in explicitly when reading from Gaussian log.


## v0.23.2 (2023-03-30)

### Bug Fixes

- Block extraction of sf angmom
  ([`ed531ac`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ed531ac00da41e5ec275c02c65fbe586a9183f67))


## v0.23.1 (2023-03-30)

### Bug Fixes

- Ordering of amfi blocks
  ([`61d3f65`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/61d3f6574a15305600d80eb30b42667c0ec3009e))

Correct loop order when reading amfi blocks from file.


## v0.23.0 (2023-03-30)

### Features

- Two-step switch
  ([`7439b21`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7439b21ea27e4d227b0f6ac0f5a694be597b03ab))

Implement --two_step flag


## v0.22.1 (2023-03-29)

### Bug Fixes

- Wrong conversion factor of normal modes coordinates
  ([`3c86ecc`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3c86ecce4ea17f37175b18612fb7bfc9e5ddc96a))

This appears to be the same issue which was resolved in the pt_fit couple of programs a year ago.
  See issue #11 in the tau repo.

- Wrong restriction of spin when writing AMFI integrals
  ([`fc53b8d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/fc53b8de6ae0175f4dd36280d86e728d063f5baa))

### Documentation

- Update
  ([`0437aeb`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0437aeb15c8b81b8026a3acb4784fc6b78fd61b5))

### Features

- Coordinate based subsetting of lvc
  ([`1456e11`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/1456e1179df7410965f298d030b68abc1340e245))

- Coupling strength
  ([`a20802e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a20802ea659b64bb636fd460f1456e2db104d94f))

- Pass q-points for limited q-space integration
  ([`8cc7ca7`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/8cc7ca7fdf6dd9a40ed058a2bfef7ca927e3f17e))

Arbitrary q-points can be selected via supplying discrete q-points or a mesh.

- Pass reference geometry for subsetting to vib
  ([`1f83eaf`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/1f83eafb2fa62427d7edd2f7c5f32d5fe2b683ef))

- Toggle traceless couplings
  ([`18f46d6`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/18f46d6b2244e6f60b68c6eac3fe00ec7160e2d6))


## v0.22.0 (2023-03-24)

### Features

- Prep for direct tau v1
  ([`3de63ac`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3de63ac059c040d6db75ff3dc0bad995dc360e09))

Tau input data is organised in one dataset each.

- Tau direct method final format
  ([`0716fd8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0716fd801aee7cbef487f2d78dab7638c8e7a15c))

Implementation of final format for new tau interface.


## v0.21.2 (2023-03-20)

### Bug Fixes

- Mclr files again
  ([`eff2d0e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/eff2d0ebc1ce6fb6c725910e734df60bc27ebc0b))


## v0.21.1 (2023-03-20)

### Bug Fixes

- Mclr files names
  ([`504c7a5`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/504c7a5a1a48c956463bbc452c656996fdd3112c))


## v0.21.0 (2023-03-19)

### Bug Fixes

- Mclr files
  ([`6a1b138`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6a1b1387e9421d93183fb68f5ad9bb5887992519))

### Features

- Improve parallelisation for gradient Molcas jobs
  ([`ebc25a4`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ebc25a405469796fc89505134066fb6777872df3))

Also clean up front-end


## v0.20.0 (2023-03-16)

### Features

- Add adjust_cutoff option for charge generation
  ([`f3fd0dc`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f3fd0dc391926592750de7ac670a1175970495ca))

implements the bond cut-off adjustment front- and back-end


## v0.19.0 (2023-03-13)

### Features

- Enable alignment of LVC model to input geometry
  ([`bbb4801`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/bbb48014ecc0b9273b3816fec4c17b6b9c9fa01f))

LVC model is aligned to input geometry by minimising RMSD.


## v0.18.0 (2023-03-07)

### Build System

- Bump suite versions
  ([`b729988`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b729988366b384dad0a0cb6dc1ab616cd48e82bf))

Include latest updates

### Features

- Parametrise lvc with frozen density gradients
  ([`7e1d03f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7e1d03f82b2a2629655c6491d87c310532f9b97b))

Also implements addition of two LVC models.


## v0.17.0 (2023-02-28)

### Features

- Store band indices and q-points
  ([`bc618fd`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/bc618fd8104fc80893d588764db7c2c8c2139743))

each vibrational normal mode is characterised by its band index and q-point

- Toggle storage of displacement vectors
  ([`55297ac`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/55297ac862f57cc4d951430c9e3b65f740ad3882))

--no-store_vecs flag disables storage of displacement vectors

### Performance Improvements

- Improved iteration over q-points
  ([`2aeb4b0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2aeb4b010ed887fdc28f729a743ac662b28fdd55))

Use of generators for lazy evaluation of modes at different q-points


## v0.16.0 (2023-02-23)

### Features

- Enable subsetting of lvc coordinates
  ([`cffcb06`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/cffcb061f78877ba179fd6d0a25dc09210fd88da))

Enable subset of specific "active atoms" all other couplings are implicitely set to zero (ghost
  atoms).


## v0.15.0 (2023-02-22)

### Features

- Enable non-primitive unit cells in phonon calc
  ([`2f40fc7`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2f40fc7e85085327c88ac8672f0ba9d812494506))

Before, using a non-primitive cell in the phonopy calculation was giving an error in the
  spin_phonon_suite vib program ... ask Meagan why!


## v0.14.0 (2023-02-21)

### Features

- Flexible mass argument
  ([`9ca06da`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9ca06da045ac6256dcffe19b497d5f11a5def712))

enables element symbols for collective isotope substitution


## v0.13.0 (2023-02-20)

### Build System

- Bump suite version
  ([`9d5c3aa`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9d5c3aab3bd5d58029c7ca637999da4a1808efcd))

include updates from all the other versions. Next time package less changes into more merge requests
  ... fingers crossed!

### Features

- Hpc style properties, wip
  ([`1cd715e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/1cd715eaa2cd58d6644c4019456cd4df17b8b986))

Compute geometry dependent LVC properties with the hpc.store interface.

- Implement new lvc interface
  ([`7fea3c1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7fea3c1ec6a71c99141dab362f1f5e5b06ce7b57))

Now with faster differentiation code and general eval interface!

### Refactoring

- Clean up code-base
  ([`450eff4`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/450eff4e2bec08e6bf2d71d00bf48fea1c6686e6))

- Strip down for autodiff, WIP
  ([`9662388`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/966238813b5130d790928f4eb6ffae835a93f7a5))

removing manual differentiation code.


## v0.12.1 (2023-01-30)

### Bug Fixes

- Allow for none integer atomic masses
  ([`6ce2b00`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6ce2b00a4e8ea30227e92950847e941fe6bf83e5))

Adapt tpye of atomic mass from int to float.

### Documentation

- Add cleaning of local scratch
  ([`fe04264`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/fe04264babf50e361a0a09747ad313caf23b33cc))

The job submission script is setup such that it cleans the workdir after running in local scratch

- Change docs to be consistent with updated molcas_suite and spin_phonon_suite
  ([`2d8a203`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2d8a203387b98cc8c972de1e9634e12a0554393a))

- Fix typo in local scratch path
  ([`0288038`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0288038fd8b9b8b46940a0acfa09c570ad2006aa))


## v0.12.0 (2022-11-04)

### Build System

- Remove need for package name in docs build
  ([`1b8b7c1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/1b8b7c1f5b61c309cf111d3e237920cae09e2e41))

### Documentation

- Updated docs
  ([`33f6571`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/33f6571e25e295224a80510b9f32ede0d22402c5))

### Features

- Make basis set definition less redundant
  ([`18d184e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/18d184e9a752234d6c54485a1e81c592e72222b6))

the dummy basis set is defined per unique atom to reduce redundancy.


## v0.11.0 (2022-10-10)

### Features

- Pyvibms output
  ([`112692e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/112692ed57f32abd7b18eb95b03b86e65f90d606))

output for pyvibms


## v0.10.1 (2022-09-29)

### Bug Fixes

- Minor bug fix
  ([`a51a3ee`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a51a3ee2dc2ea021420e02792879abf9eebe8c0b))

syntax error


## v0.10.0 (2022-09-29)

### Build System

- Bump molcas_suite version
  ([`37ca0ec`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/37ca0ec22ad55ed865fedbd7c3d50ea06fca72d2))

Newer molcas_suite version reflects recent changes for correct behaviour of lvc input generation.

### Documentation

- Update documentation
  ([`89a6ad4`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/89a6ad4529d95ee2ec92047c844e22ae70a80381))

Document refined workflow.

### Refactoring

- Fix bugs
  ([`fd2517d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/fd2517db7a3c0494c8d4eed15420e9fabbecda1c))

Fix some bugs introduced in previous commit.

- Output supercell coordinates with env basis labels
  ([`7150db6`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7150db676cf33331437079f3a12ba50bfff7da32))

Environment atoms carry predefined environment basis labels.


## v0.9.1 (2022-09-22)

### Bug Fixes

- Add xyz file number of atoms and comment lines
  ([`2c0d8c7`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2c0d8c7569024ee18638b644471636a788398987))

### Build System

- Add molcas_suite dep
  ([`4c94430`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4c94430f400c63905379a9357d4cee51951f2fe1))

### Documentation

- Fix code indentation
  ([`cf8a232`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/cf8a23279fdf2f0c96837eb3f89a108f7c2be6df))

- Update gen_charges sample output
  ([`e2044a2`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e2044a28352622177deb46cce23fd00f3a7e3e0a))


## v0.9.0 (2022-09-01)


## v0.8.0 (2022-09-01)

### Build System

- Add molvis dep
  ([`d72cbc4`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d72cbc401f1aa489105e8e3e2232a9b003eeaf94))

- Add molvis dependency
  ([`48b516a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/48b516a2f9849bbc905a231447a0489e3c313feb))

### Documentation

- Update environment page to be clearer and add note on visualisation
  ([`b4d0e46`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b4d0e465c5ff8659a4381ff33baafc401c245e0e))

### Features

- Add charge visualisation
  ([`a9e7d33`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a9e7d33d2e63fbd0797c52031ce73ca6d1827f1a))

- Add charges viewer code
  ([`dbb3ffa`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/dbb3ffa7d1f24925a6ee8ed641ecb82d74c10cb1))

- Add vis_charges cli arg
  ([`96ec363`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/96ec3636a07232c6e28e10486718faeb21051209))

- Add visualisation
  ([`07bf1f3`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/07bf1f3c8e72147e4bd8ef1b18e3ce4212adffd9))

- Add visualisation options to parse_charges
  ([`c096e9f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c096e9f82f65daabe846cdadc1bd915d3c99efbe))


## v0.7.1 (2022-08-31)

### Build System

- Bump xyz_py vers
  ([`4984900`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4984900cb6b9766134a8e35d380ea9d23ce37b76))

### Documentation

- Change basis set specification file to ENV, add comment, and update help
  ([`765c102`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/765c102d04897fb57ab521814c7f948055a5ac79))

- Update equilibrium section for charges, and names of files in environment section
  ([`e457a66`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e457a665fdf1a445bf7705e8654724561a23e6b4))


## v0.7.0 (2022-08-26)

### Build System

- Add sleep to give pip time to see new version
  ([`e6314d5`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e6314d59f0d52354f45a5b983410ded2ca5112be))

### Documentation

- Add cells module
  ([`7f64c4c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7f64c4c7aeca87e8a1f39902e6546324ee22eae2))

- Add deletion of old docs badge
  ([`b3f848b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b3f848bd0c83eb8eede4167ddca57d0224f6e493))

- Add information on output files of parse_charges
  ([`204e6c1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/204e6c1ad844ae05f57a586c5f02d89b5128d336))

### Features

- Add molcas basis set output for environment charges
  ([`254956c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/254956ca82e7fe54695366632062726b540abaa6))


## v0.6.0 (2022-08-26)

### Documentation

- Add note for forces supercell expansion
  ([`174dca8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/174dca8ae59f30fbea4c2a4d7cb115e84bcce1d4))

### Features

- Add --force_expansion arg to indicate phonon supercell expansion
  ([`1acc876`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/1acc876d8ed8cfbbe8d382df9bc60a9ca7881c11))

### Refactoring

- New default for num_steps
  ([`7126f31`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7126f31c9e562dff8f28a8d70003d7fc4f42203c))

now only calculated central point


## v0.5.0 (2022-08-25)

### Features

- Readd printing of supercell_charges.dat
  ([`3a26160`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3a261600939c9a8c9975eb5477c988c209302aa9))

supercell_charges.dat is still needed in current workflow.


## v0.4.1 (2022-08-25)

### Bug Fixes

- Name of cli interface with underscores
  ([`6d87e79`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6d87e79f374b0622eb1b52f0a10b9eb8fc18eac6))

Replacing the first underscore with a dash was maybe not the best idea...


## v0.4.0 (2022-08-25)

### Build System

- Add docs badge
  ([`3bc9271`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3bc9271cad5c8d9b13794b2d2755940785263ba9))

- Add missing comma
  ([`292aab4`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/292aab426695782331da2e81f4528e8213c6af55))

- Add phonopy
  ([`92c7bde`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/92c7bdea2b19c78993540c23ec27dc7d4f0ce5de))

- Add vers number to docs
  ([`2504cd0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2504cd01b8f0feb58bf723ebe098fc4d7f71a4c1))

- Update file path
  ([`500a7a9`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/500a7a9db4aaf90fd43d6453ce870bd34b14e4db))

### Documentation

- Rearrange environment step
  ([`41b5adf`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/41b5adfe7d866431b5069bee702c888f58900f5d))

- Update environment
  ([`f17b53f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f17b53f196bda9d4df74e59405fa722b714cfa05))

- Update environment
  ([`78b68c8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/78b68c8303471b44a5fbc39a291e332d50a05fee))


## v0.3.1 (2022-08-23)

### Bug Fixes

- Remove merge conflict
  ([`3c43f0b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3c43f0bf58dc483ca521f9fc940906a88f8e39a0))

### Documentation

- Fix formatting
  ([`59a1e28`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/59a1e28c9d734f3d9f4ac89a678b927b185d5f41))


## v0.3.0 (2022-08-23)

### Documentation

- Improve documentations
  ([`83cdfb1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/83cdfb192221427d34e2f420bfd855bd24731c72))

minor changes

### Features

- Commensurate q points now implemented
  ([`9bed548`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9bed548a641536260783063f0731d13051f834c6))

phonopy interface now producing vibrations at commensurate q points.

- Polish phonopy interface
  ([`c2ae306`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c2ae3069f695084ba68ae6bc026bc6bfc0b3fbaf))

cosmetic changes


## v0.2.0 (2022-08-23)

### Bug Fixes

- Add hydrogen atom to basis set defs
  ([`fef473a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/fef473a619c3be788c2a11ddac8fafa92201ba52))

- Add missing expansion attribute to SuperCell
  ([`cbd1a32`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/cbd1a3284c59e64e3aaed66625d472081d87413d))

- Correct bug in lattice vector row vs column major
  ([`03cafda`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/03cafda67562a3154c481b72d2ebfef0929564bc))

### Build System

- Add override for docs only changes
  ([`0aa6770`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0aa6770cacc2838eb9434ef0469fb3abc0f7a5e4))

- Bump python vers and add gaussian_suite and xyz_py dependencies
  ([`040802f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/040802fcc3d1539ac01aae6dac9ee81587a1dcc9))

### Documentation

- Add coming soon
  ([`e6e499d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e6e499dbece35f8127045012010b013284a7e9f0))

- Add description to charge subparsers
  ([`2fc7011`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2fc701164f06bdeee0f5986f6e6206c5f017085f))

- Add environment guide
  ([`4e76bd2`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4e76bd28791625b8f6a1fc0f022f965f88df44de))

- Correct grammar
  ([`28d735e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/28d735e5b4eee8118b041ac855447f91c803b3d4))

- Indentation error
  ([`f6dbc2c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f6dbc2c2abc0f87a1aef7b5d22f6c99c60f8b757))

fix syntax error in frequencies.rst

- Update docstrings
  ([`4f047b1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4f047b1e85ea55e7906fdc60551b215601b8c31a))

- Update environment docs
  ([`d9d3b91`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d9d3b9179fed45bc605a36ae72132d677cb37a5d))

- Update help for parse_charges
  ([`3422d9e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3422d9ee58539f7de80f7b6408cc4885e184d9bb))

### Features

- Add new basis set and ecp defs
  ([`60b554f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/60b554f15c56eeae1595aa5003d4b115fe055d0f))

- Add repaired xyz file printing
  ([`b34a026`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b34a026bed69aacbb7b15eff555f0eaf9953419d))

- Add supercell charges code
  ([`5afff8a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/5afff8aedea65a4bd0d45f560e0b55235d2cf233))

- Add supercells class and remove use of phonopy for supercell construction
  ([`3ca112a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3ca112a96c197e0d4bc6e78956b336f6dca54b93))

- Add warning for high symmetry systems
  ([`75aebbb`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/75aebbb612f8a1ec9de94e0b7506248ee8eaf9c1))

- Adjust phases
  ([`9e333f7`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/9e333f79735797a05d93fe65776df6dc2fb37c6c))

phases are adjusted such that the diagonal has positive signs.

### Performance Improvements

- Replace old frac --> cart conversion
  ([`dbe52ea`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/dbe52ea8931f2e6b291dc8ad319488193fc8a055))

- Simplify file names and handling
  ([`533b02d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/533b02d7bd531ce8a3b581591b305c1aaa2b65e1))

- Use mic distances to check for repairs
  ([`e8e2fc6`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/e8e2fc67bf8b6fdf223b021c4bd68d127e72d062))

### Refactoring

- Change name of asu xyz to match potential non-molecule output
  ([`7f66718`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7f66718f3d6559d478b553cea3b8f8bc06ad6356))

- Improve pep8 compliance
  ([`a34c7e1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a34c7e16abb33262ea7df8ed1ec140af84081b56))

- Remove old phonopy calls
  ([`4cefbff`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4cefbffa11c323eab37f8c67a19d860dc4b7a2a7))

- Remove old phonopy code
  ([`72a9625`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/72a9625b0baee3e87f49b42deb376b6116ae0cd5))

- Simplify coord handling
  ([`3c1a2b1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3c1a2b1e2d0ec02ba73f6d780635a236c9f797f9))


## v0.1.1 (2022-08-19)

### Bug Fixes

- Clean up phonopy interface
  ([`f26e781`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f26e7812f6542d88382c19938c42cb27503caed4))

Also includes fixes introduced in prev commit.

- Rename cli interface to be spin-phonon_suite
  ([`4279835`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/42798351959d9fba81c9ef07521273bff064a013))

Before it was snake_case all the way

### Build System

- Add docs only force
  ([`f1060bf`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f1060bf5d576125d0020b1e1dbab8e888c7b3c49))

- Add gaussian_suite to dependencies
  ([`74f151c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/74f151c5e2d8a87ce3d7b796afffe1dc9255b2a2))

- Force reinstall of package to avoid incorrect version
  ([`595364b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/595364b2040a5eab0175cf48c6c3995d29a4ec6b))

- New docs build process
  ([`f825f84`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f825f8458e51ec22af8893b7fff04744d4c93de2))

- Remove pdoc3
  ([`cf10533`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/cf10533c4188ccbff182d3bd8a02897a1c255c58))

### Documentation

- Add sphinx based documentation
  ([`b92c5e1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b92c5e10e3348d1a81b4dabfd9001801e648de41))

- Update docs to use local copy
  ([`0fa4ba3`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0fa4ba34436b6e780975f69005fa18206bbef707))

### Features

- Add phonopy interface
  ([`4c5fd87`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4c5fd87d2c821294682eff2d205560df2e587c44))

Now testing.

- Work on phonopy interface, wip
  ([`23327ed`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/23327edd985be76b3f09b2a91d8301171dc5b63b))

Normal modes need to be expanded to supercell.

### Refactoring

- Clean up phonopy interface
  ([`15f7072`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/15f7072e44ca7b62e3aba73caf73291de77fcda2))

unit cell eigen vectors are there


## v0.1.0 (2022-08-19)

### Build System

- Bump angmom_suite
  ([`fdd8203`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/fdd820371c6625efdda7b5ffe8e0a0d9e92bf39a))

### Features

- Add spin phonon cupling strength plotting
  ([`d8c4a65`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d8c4a65c0fd846a1a7f350de7060d336ff3ec249))


## v0.0.2 (2022-06-21)

### Bug Fixes

- Active atom indexing
  ([`016884d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/016884dae85e85b2043fc110e9004d5e5e60c490))

make active atom indices one-based for input.


## v0.0.1 (2022-06-09)

### Bug Fixes

- Bug-fix in input file generation
  ([`4cb5f9b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/4cb5f9bdcde3a635cc47b79a4165b78d3735a278))

Fix integer string conversion.

- Bug-fixes in handling of distortions
  ([`68ce593`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/68ce59394d4db4ca87afe2d18bf85552094fdc77))

Update distort code. Load whole step size array.

- Correct handling of max_order arguments
  ([`76826b8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/76826b875b864d412eaefc45f54e05f0e870087c))

set max_orders to zero if None.

- Correction in sf2ws_spin
  ([`35d158e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/35d158e3853146b231f9efb1193f72648eb0343a))

Adjust special case for singlet states in coefficient evaluation.

- Fix of phasing and block ordering issues
  ([`adb79b0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/adb79b024a71a1ebf771e6f6aa37447df975c8a4))

Simultaneous phasing of spin and angm operators now solved by block-wise phasing.

- Improvement in LVC/CFP code
  ([`d8d040b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d8d040b3de0193fa2ddadaa16fe20c297be45931))

Spin-orbit Hamiltonian is now referenced to spin-free GS energy. Symbol to specify manifold for CFP
  projection is can now be term symbol, multiplet symbol or J quantum number only.

- Input_generator bug
  ([`220339a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/220339a4f178065052af36db18b63aba465a0e5b))

Fixes bug in input generator which inserted dublicate section. Now initialises list to empty list in
  every iteration.

- Input_generator bug
  ([`bcf8060`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/bcf80603502c55ebef9a8c6776e0d71b985c7345))

Fixes bug in input generator which inserted dublicate section. Now initialises list to empty list in
  every iteration.

- Minor updates
  ([`ce42f1b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ce42f1bb57679248392921c575c6abab85ac0fb0))

inverse conditional to check for coords

- Minor updates
  ([`39b96aa`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/39b96aa4cb3d06226eabdf7ce21e29c892bde002))

inverse conditional to check for coords

- Verbose matrix printing bug
  ([`70a304a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/70a304a388775fd1b4aea0ca18cdffce46032d20))

Fix matrix printing condition for the first derivative of the transformation.

### Build System

- Siwtch to pypi for distribution
  ([`b4a63bb`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/b4a63bb1051133b5989895c34d3de4e1072d0fd6))

- Switch master to main
  ([`15ed382`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/15ed3820bcdadd27f9b7935c82c4ebe734b6f02b))

- Update angmom-suite dependency
  ([`acf54a0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/acf54a06a701ee67f52f0167e39a6106b5fd46c4))

add angmom-suite dependency to setup.sh

- Update dependencies
  ([`280335f`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/280335fa58dbde0dbc5eaa856dd49a25c1bb3e0d))

Remove inorgqm dependency and relax scipy version.

- Update dependency requirement
  ([`7f8086c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7f8086ca70827a5116ef041ede96ef8046c502fd))

Add dependencies to setup.py and lower python requirement by replacing the usage of the prod
  function of the math module.

- Update version location
  ([`91d0123`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/91d0123ce848ae9e6dbdc767f243e74d18c2d3f0))

### Documentation

- Add missing backtick
  ([`c653bf2`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c653bf2c92e017ee46699bcf294d25660c37404c))

- Pep8 compliance
  ([`153965a`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/153965a50d9c2547f9178f98e7ecbb106eb86a88))

- Remove incorrect comment about Dy hard coding
  ([`adde5ce`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/adde5ce0c5de0fbf02131e76afb4a20d7b0d9a46))

- Update readme with new suite installation methods
  ([`279932b`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/279932b6370163f12446f5423c0d3728d4fae9b3))

### Features

- Add alaska options for lvc inputs
  ([`ce85501`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/ce8550103005679f4701b83ea66378c19cb4b36a))

add alaska keyword options

- Add hessian diagonalisation code for force constants
  ([`0795e13`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0795e138aade65752aa21aac5905e93b10b0144d))

- Add hessian matrix diagonalisation code
  ([`6406e2c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6406e2c224285bc81883954b66a146b15ea14d95))

- Add max_analytic_order argument
  ([`efa7dcc`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/efa7dcc13746e3c8d6cffcd828597d10cbc2191f))

Maximum analytic order can be now controlled by the max_analytic_order argument to spin-phonon_suite
  derivative.

- Add max_analytic_order argument
  ([`7da26f0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/7da26f05d34108422d68496feeacff50ccffc1f8))

Maximum analytic order can be now controlled by the max_analytic_order argument to spin-phonon_suite
  derivative.

- Add mclr option
  ([`6e300c0`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/6e300c08da93e52d15b6dd63cac76a3e80e56667))

cli option to add explicit invocation of MCLR module.

- Add multiplicity to `molecules` argument
  ([`3de7960`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/3de79607d1a236dcc8bc3fda18e40c6e69a415a6))

- Add progress print out
  ([`0ab266c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0ab266cb375a5407dd1d079ab606f55be7bf48cb))

Prints current distortion/derivative.

- Add progress print out
  ([`37f9afd`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/37f9afdb6fc76149a0f7be8d15069340bb48ecf4))

Prints current distortion/derivative.

- Add user argument for number of rassi states replacing hard coded Dy
  ([`947d66d`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/947d66d520daa2bc7ebf731b20fef0ad40eca7db))

- Enable choice to project out translations and rotations
  ([`00a4b59`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/00a4b59209ccd42d729f3aa18a235e83b84b554d))

The boolean optional arguments --trans and --rot are provided.

- Gradient norm and invariance printing
  ([`46def28`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/46def28acdb81b268abc26ab6b26e2853f727ff2))

Plot matrices of gradient norm and translational as well as rotational invariance.

- Implement prep tau part
  ([`99cbc8e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/99cbc8e90b33b9ed15087d5fdd55e9cb411150ca))

Implement preparation wrapper for tau program.

- Setup of vibration database
  ([`d6a4ae3`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/d6a4ae3bdd5f862b145ff62be520cfdede859053))

Add setup of hdf5 file containing all vibrational information.

- Support of active atoms in the coupling calculation
  ([`aa7a94c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/aa7a94cff6dac1633119c58df6457c8fa8ea843b))

Update transformation code and enable the usage of active atoms in the spin-phonon coupling
  calculation.

### Refactoring

- Adapt and clean up function backend
  ([`44abe61`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/44abe614460e754f83444309bda07894b2067ec8))

Finite distortion backend works now.

- Adapt usage of new angmom_suite package
  ([`eee598e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/eee598e7c6b44ebf010e6e3d26c45d44a48d2e58))

Change imports and out-source make_spin function.

- Adjust path of hdf5 reader
  ([`f315cc3`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/f315cc3e477d147c6bca2a4b6e21a14a1fafa36c))

Removes "distortions" level from hdf5 reader hierarchie.

- Clean up
  ([`c569239`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c569239a62ebc7eb6cc77848187c72d9744d8988))

Clean up of code.

- Clean up basis selection
  ([`85de3a1`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/85de3a1862f1e74eaf64e125a2803ebf2000b6ba))

The Stephens operator projection code now takes the arguments "term" and "multiplicity" along with
  the basis labels to choose the type of model angm Hamiltonian.

- Clean up lvc and func
  ([`31329eb`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/31329eb42e3437ffc2e2a4a7019ff73646500be3))

Tau-style derivative calculation usable now.

- Clean up lvc input generation
  ([`a231ae3`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a231ae38ebb47a8a08f320e83058acf995284063))

Additative generation of alaska/mclr input sections.

- Cosmetic changes
  ([`73d368c`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/73d368cc19b8d72c4274d5517322168a3853dd69))

Adjust cli parameter naming convention.

- Establish naming precedence for intermediate files
  ([`a4b8e38`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a4b8e3855ece467a2342945d70acfb5e6e6aa442))

distortion_info, vib_data, lvc_data and grid_data now folling consistent convention and flag
  letters.

- Improve flake8 compliance
  ([`af9679e`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/af9679e67922f48f299ee5dddd7819a1edde1fde))

Addition of doc strings and improvements in general formatting.

- Modernise lvc code
  ([`a1cb6eb`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/a1cb6ebdc05dd64cee1dfd02cc94e123c29877b4))

The LVC code needs refactoring after major changes in molcas_suite extractor and stevens CFP
  projection code.

- More flexible function specification
  ([`8531070`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/8531070233f047cae7b43714bc6aeda1f031911d))

Function can now be supplied with (kw)args to allow for theta correction for tau-style printing.

- Out-source stevens op code
  ([`0eae2ad`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/0eae2ad3b247d8adac59c5b39f53af092d2368d3))

Stevens operator projection code finds a new home in angmom-suite.

- Overhaul stevens operator code to work with S2L2SzLz basis
  ([`c5513de`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/c5513decadd209b8855b3a8f14f65f7912b1273f))

Add angm quantum number and projection clean up and checks. Adjust interface for stevens operator
  projection code to use term symbol to specify term/multiplet and divorce operators and their
  derivatives. Clean up.

BREAKING CHANGE: change in interface

Change interface of project_CF() function

- Set up CFP derivative transformation and printing
  ([`2f8ec83`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/2f8ec83a8fe7fa32d87ff36da728e1f6c0a81bd6))

WIP

- Use key_only class for alaska/mclr input
  ([`92474b8`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/92474b8508e749dafbbd91906f655cb2375ed1ce))

Enables the uasage of keyword only arguments.

- Use new format of AMFI integrals
  ([`73de198`](https://gitlab.com/chilton-group/spin_phonon_suite/-/commit/73de1986ed132d29056bd5fc9e8badd897468a7d))

The format of AMFI integrals has been changed to transposed layout in Molcas extractor. This leads
  to a cleaner computation of spin-orbit coupling.
